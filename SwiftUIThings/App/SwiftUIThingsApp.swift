//
//  SwiftUIThingsApp.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 22/3/22.
//

import SwiftUI

@main
struct SwiftUIThingsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
