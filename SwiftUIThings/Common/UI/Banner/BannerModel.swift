//
//  BannerModel.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 24/6/22.
//

import SwiftUI

struct BannerModel {
    let title: String
    let subtitle: String?
    let image: Image?
    let mode: BannerMode
    let duration: TimeInterval
    let didTap: (() -> ())?
    
    init(
        title: String,
        subtitle: String? = nil,
        image: Image? = nil,
        mode: BannerMode = .success,
        duration: TimeInterval = 5,
        didTap: (() -> ())? = nil
    ) {
        self.title = title
        self.subtitle = subtitle
        self.image = image
        self.mode = mode
        self.duration = duration
        self.didTap = didTap
    }
}

enum BannerMode {
    case info
    case warning
    case success
    case error
    
    func getColor() -> Color {
        switch self {
            case .info:
                return .blue
            case .warning:
                return .yellow
            case .success:
                return .green
            case .error:
                return .red
        }
    }
}
