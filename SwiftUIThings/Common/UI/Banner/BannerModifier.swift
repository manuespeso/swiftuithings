//
//  BannerModifier.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 24/6/22.
//

import SwiftUI

struct BannerModifier: ViewModifier {
    
    @Binding private var isShowing: Bool
    private let model: BannerModel
    
    init(isShowing: Binding<Bool>, model: BannerModel) {
        self._isShowing = isShowing
        self.model = model
    }
    
    func body(content: Content) -> some View {
        ZStack {
            content
            if isShowing {
                VStack {
                    Banner()
                    Spacer()
                }
                .padding()
                .transition(AnyTransition.move(edge: .top).combined(with: .opacity))
                .onTapGesture {
                    dismissBanner()
                    model.didTap?()
                }
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + model.duration) {
                        dismissBanner()
                    }
                }
            }
        }
        .animation(.easeInOut, value: isShowing)
    }
    
    private func Banner() -> some View {
        HStack {
            model.image?
                .foregroundColor(.white)
            VStack {
                Text(model.title)
                    .font(.title3.bold())
                    .foregroundColor(.white)
                    .lineLimit(nil)
                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 4)
                if let subtitle = model.subtitle {
                    Text(subtitle)
                        .font(.body)
                        .foregroundColor(.white)
                        .lineLimit(nil)
                        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                }
            }
            .padding(.leading, 4)
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .padding()
        .background(model.mode.getColor())
        .cornerRadius(8)
        .shadow(radius: 8)
    }
    
    private func dismissBanner() {
        withAnimation {
            self.isShowing = false
        }
    }
}
