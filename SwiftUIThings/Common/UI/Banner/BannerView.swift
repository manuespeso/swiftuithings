//
//  BannerView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 24/6/22.
//

import SwiftUI

struct BannerView: View {
    
    @State private var showingBanner: Bool = false
    
    var body: some View {
        Text("Tap me!")
            .onTapGesture {
                self.showingBanner = true
            }
            .banner(
                isShowing: $showingBanner,
                model: .init(
                    title: "Hello Title",
                    subtitle: "Hello Subtitle",
                    image: Image(systemName: "heart.fill"),
                    mode: .warning,
                    didTap: {
                        print("Dismiss!")
                    }
                )
            )
            .defaultNavigationBarStyle()
    }
}

struct BannerView_Previews: PreviewProvider {
    static var previews: some View {
        BannerView()
    }
}
