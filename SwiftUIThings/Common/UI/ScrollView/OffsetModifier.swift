//
//  OffsetModifier.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 3/4/22.
//

import SwiftUI

struct OffsetModifier: ViewModifier {
    
    @Binding private var offset: CGFloat
    
    init(offset: Binding<CGFloat>) {
        self._offset = offset
    }
    
    func body(content: Content) -> some View {
        content
            .overlay {
                GeometryReader { proxy in
                    let minY = proxy.frame(in: .named("SCROLL")).minY
                    
                    Color
                        .clear
                        .preference(key: OffsetKey.self, value: minY)
                }
                .onPreferenceChange(OffsetKey.self) { minY in self.offset = minY }
            }
    }
}

struct OffsetKey: PreferenceKey {
    static let defaultValue: CGFloat = 0
    
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
    }
}
