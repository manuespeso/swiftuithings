//
//  PasswordTextField.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 26/3/22.
//

import SwiftUI

struct PasswordTextField: View {
    
    @Binding var password: String
    @State private var showPassword = false
    
    private let darkGray = Color(red: 41/255, green: 42/255, blue: 48/255)
    private let clearGray = Color(red: 181/255, green: 182/255, blue: 183/255)
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 28)
                .frame(height: 75, alignment: .center)
                .foregroundColor(darkGray)
            
            HStack {
                if !showPassword { Spacer() }
                RoundedRectangle(cornerRadius: showPassword ? 28 : 50)
                    .frame(
                        width: showPassword ? nil : 50,
                        height: showPassword ? 75 : 50, alignment: .center
                    )
                    .animation(.linear(duration: 0.2), value: showPassword)
                    .foregroundColor(clearGray)
                    .padding(.trailing, showPassword ? 0 : 12)
            }
            
            HStack {
                if showPassword {
                    TextField("Password", text: $password)
                        .font(.headline)
                        .foregroundColor(darkGray)
                        .padding(.leading, 20)
                } else {
                    SecureField("Password", text: $password)
                        .foregroundColor(clearGray)
                        .padding(.leading, 20)
                }
                Spacer()
                Image(systemName: showPassword ? "eye" : "eye.slash")
                    .resizable()
                    .frame(width: 32, height: 20)
                    .font(.system(size: 16, weight: .bold))
                    .foregroundColor(darkGray)
                    .padding(.trailing, 20)
                    .onTapGesture { showPassword.toggle() }
            }
        }
    }
}

struct PasswordTextField_Previews: PreviewProvider {
    static var previews: some View {
        PasswordTextField(password: .constant("text input"))
    }
}
