
import SwiftUI

extension Color {
    
    static var darknessPurple: Color {
        UIColorFromHex(rgbValue: 0x252254)
    }
    
    static var darknessWhite: Color {
        UIColorFromHex(rgbValue: 0xEAF0E7)
    }
    
    static private func UIColorFromHex(rgbValue: UInt32, alpha: Double = 1.0) -> Color {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0xFF) / 255.0
        
        return Color(.sRGB, red: red, green: green, blue: blue, opacity: alpha)
    }
}
