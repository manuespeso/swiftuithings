//
//  View+Banner.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 24/6/22.
//

import SwiftUI

extension View {
    
    func banner(isShowing: Binding<Bool>, model: BannerModel) -> some View {
        modifier(BannerModifier(isShowing: isShowing, model: model))
    }
}
