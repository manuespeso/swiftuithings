//
//  View + DefaultNavBar.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 24/6/22.
//

import SwiftUI

extension View {
    func defaultNavigationBarStyle(with title: String = "") -> some View {
        modifier( DefaultNavigationBar(title: title) )
    }
}

fileprivate struct DefaultNavigationBar: ViewModifier {
    var title: String
    
    func body(content: Content) -> some View {
        content
        .navigationTitle(title)
        .navigationBarTitleDisplayMode(.inline)
    }
}
