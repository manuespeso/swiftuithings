//
//  ContentModel.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 3/4/22.
//

import SwiftUI

struct ContentModel: Identifiable {
    let id = UUID()
    let name: String
    let view: AnyView
}
