//
//  Movie.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 1/4/22.
//

import Foundation

struct Movie: Identifiable {
    let id = UUID().uuidString
    let movieTitle: String
    let artwork: String
}
