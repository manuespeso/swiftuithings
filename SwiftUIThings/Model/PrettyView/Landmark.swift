
struct Landmark {
    let id: Int
    let name: String
    let park: String
    let state: String
    let description: String
    let imageName: String
    let latitude: Double
    let longitude: Double
}
