//
//  ContentView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 22/3/22.
//

import SwiftUI

struct ContentView: View {
    
    @State private var userComments: String = ""
    
    private let content: [ContentModel] = [
        ContentModel(
            name: "Pretty View",
            view: AnyView(
                PrettyView(
                    landmark: Landmark(
                        id: 1,
                        name: "Quilimanjaro",
                        park: "Montañas montañosas",
                        state: "Abierto",
                        description: "Este parque está muy chulo bla bla bla",
                        imageName: "prettyImage",
                        latitude: 34.011_286,
                        longitude: -116.166_868
                    )
                )
            )
        ),
        ContentModel(name: "Reometry reader demo", view: AnyView(GeometryReaderDemo())),
        ContentModel(name: "Wordle", view: AnyView(Wordle())),
        ContentModel(name: "Carousel/Hero animation", view: AnyView(HeroView())),
        ContentModel(name: "Messages gradient demo", view: AnyView(MessagesGradientDemo())),
        ContentModel(name: "Duolingo", view: AnyView(DuolingoView())),
        ContentModel(name: "Custom Banner", view: AnyView(BannerView())),
        ContentModel(name: "Parallax Effect", view: AnyView(ParallaxView())),
        ContentModel(name: "Custom Toolbar", view: AnyView(CustomToolbarView())),
    ]
    
    init() {
        UITableView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.purple
                VStack(spacing: 0) {
                    Text("Prueba")
                        .font(.body)
                        .bold()
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .padding()
                    TextEditor(text: $userComments)
                        .frame(maxWidth: .infinity, maxHeight: 150)
                        .cornerRadius(8)
                        .foregroundColor(.black)
                        .accentColor(.blue)
                        .padding(.horizontal, 8)
                    //Spacer() // Sin este Spacer y List, el VStack se centraria
                    Spacer().frame(height: 16)
                    Section(header: createListHeader()) {
                        List(content) { item in
                            NavigationLink(destination: item.view) {
                                Text(item.name)
                            }
                            .listRowBackground(Color.green)
                        }
                        .listStyle(.plain)
                    }
                }
            }
            .navigationBarHidden(true)
        }
        .onAppear { UIScrollView.appearance().bounces = false }
    }
    
    private func createListHeader() -> some View {
        VStack(alignment: .leading) {
            Text("Header line 1")
                .multilineTextAlignment(.center)
            Text("This is header line 2")
                .multilineTextAlignment(.center)
        }
        .frame(maxWidth: .infinity, alignment: .center)
        .background()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
