
import SwiftUI

struct CustomToolbarModel: Identifiable {
    let id = UUID().uuidString
    let icon: String
    let name: String
    let color: Color
    var toolPosition: CGRect = .zero
}
