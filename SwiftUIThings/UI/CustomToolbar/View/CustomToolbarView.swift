//
//  CustomToolbarView.swift
//  SwiftUIThings
//
//  Created by Manu Espeso on 26/9/22.
//

import SwiftUI

struct CustomToolbarView: View {
    
    // MARK: - Properties
    
    @State private var tools: [CustomToolbarModel] = [
        .init(icon: "scribble.variable", name: "Scribble", color: .purple),
        .init(icon: "lasso", name: "Lasso", color: .green),
        .init(icon: "plus.bubble", name: "Comment", color: .blue),
        .init(icon: "alt", name: "Enhance", color: .orange),
        .init(icon: "paintbrush.pointed.fill", name: "Picker", color: .pink),
        .init(icon: "rotate.3d", name: "Rotate", color: .indigo),
        .init(icon: "gear.badge.questionmark", name: "Settings", color: .yellow)
    ]
    @State private var activeTool: CustomToolbarModel?
    @State private var startedToolPosition: CGRect = .zero
    
    // MARK: - Body
    
    var body: some View {
        VStack {
            VStack(alignment:.leading, spacing: 12) {
                ForEach($tools) {
                    ToolBarView(bindingTool: $0)
                }
            }
            .padding([.vertical, .horizontal], 10)
            .background {
                RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .fill(.white)
                    .shadow(color: .black.opacity(0.2), radius: 6)
                /**
                 ```Limiting background size```
                 Image Size -> 45 + Horizontal Padding -> 22
                 Total = 66
                 */
                    .frame(width: 66)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            .coordinateSpace(name: "AREA")
            .gesture(
                DragGesture(minimumDistance: 0)
                    .onChanged({ value in
                        guard let firstTool = tools.first else { return }
                        
                        if startedToolPosition == .zero {
                            startedToolPosition = firstTool.toolPosition
                        }
                        
                        let location = CGPoint(x: startedToolPosition.midX, y: value.location.y)
                        if let index = tools.firstIndex(where: { $0.toolPosition.contains(location) }),
                           activeTool?.id != tools[index].id {
                            withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 1, blendDuration: 1)) {
                                activeTool = tools[index]
                            }
                        }
                    })
                    .onEnded({ _ in
                        withAnimation(.interactiveSpring(response: 0.5, dampingFraction: 1, blendDuration: 1)) {
                            activeTool = nil
                            startedToolPosition = .zero
                        }
                    })
            )
        }
        .padding(16)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationTitle("Toolbar Animation")
    }
    
    // MARK: - Toolbar View
    
    private func ToolBarView(bindingTool: Binding<CustomToolbarModel>) -> some View {
        let tool = bindingTool.wrappedValue
        
        return HStack(spacing: 6) {
            Image(systemName: tool.icon)
                .font(.title2)
                .foregroundColor(.white)
                .frame(width: 45, height: 45)
                .padding(.leading, activeTool?.id == tool.id ? 5 : 0)
                .background { ToolbarImageBackground(bindingTool) }
            
            if activeTool?.id == tool.id {
                Text(tool.name)
                    .padding(.trailing, 16)
                    .foregroundColor(.white)
            }
        }
        .background {
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .fill(tool.color)
        }
        /**
         ```Moving x offset```
         Image Size -> 45 + Trailing Padding -> 11 + Extra -> 5
         Total = 61
         */
        .offset(x: activeTool?.id == tool.id ? 61 : 0)
    }
    
    private func ToolbarImageBackground(_ bindingTool: Binding<CustomToolbarModel>) -> some View {
        GeometryReader { proxy in
            let frame = proxy.frame(in: .named("AREA"))
            
            Color
                .clear
                .onAppear(perform: { bindingTool.wrappedValue.toolPosition = frame })
        }
    }
}

struct CustomToolbarView_Previews: PreviewProvider {
    static var previews: some View {
        CustomToolbarView()
    }
}
