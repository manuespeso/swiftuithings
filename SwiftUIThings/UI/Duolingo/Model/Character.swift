//
//  Character.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 23/6/22.
//

import UIKit

struct Character: Identifiable, Hashable, Equatable {
    
    static let mockCharacters: [Character] = [
        .init(value: "Lorem"), .init(value: "Ipsum"),
        .init(value: "is"), .init(value: "simply"),
        .init(value: " dummy"), .init(value: "text"),
        .init(value: "of"), .init(value: "the"),
        .init(value: "design")
    ]
    
    let id = UUID().uuidString
    let value: String
    let padding: CGFloat = 10
    var textSize: CGFloat = .zero
    let fontSize: CGFloat = 19
    var isShowing: Bool = false
    
    mutating func uploadTextSize(_ newSize: CGFloat) {
        self.textSize = newSize
    }
}
