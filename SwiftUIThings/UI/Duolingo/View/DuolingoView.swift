//
//  DuolingoView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 23/6/22.
//

import SwiftUI

struct DuolingoView: View {
    
    @State private var progress: CGFloat = 0
    @State private var characters = Character.mockCharacters
    @State private var animateWrongText: Bool = false
    @State private var droppedCount: CGFloat = 0
    
    // MARK: - Drag&Drop Grid Arrays
    ///   `Drag`
    @State private var shuffledRows = [[Character]]()
    ///   `Drop`
    @State private var rows = [[Character]]()
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 14) {
                HeartNavBar()
                HeaderArea()
                DropArea()
                    .padding(.vertical, 30)
                DragArea()
            }
        }
        .padding()
        .defaultNavigationBarStyle()
        .onAppear {
            // Shuffled
            characters = characters.shuffled()
            shuffledRows = generateGrid()
            // Normal
            characters = Character.mockCharacters
            rows = generateGrid()
        }
        .offset(x: animateWrongText ? -30 : 0)
    }
    
    // MARK: - Custom NavBar
    
    private func HeartNavBar() -> some View {
        HStack(spacing: 18) {
            Button {
                
            } label: {
                Image(systemName: "xmark")
                    .font(.title3)
                    .foregroundColor(.gray)
            }
            
            GeometryReader { proxy in
                ZStack(alignment: .leading) {
                    Capsule()
                        .fill(.gray.opacity(0.25))
                    Capsule()
                        .fill(.green)
                        .frame(width: proxy.size.width * progress)
                }
            }
            
            Button {
                
            } label: {
                Image(systemName: "suit.heart.fill")
                    .font(.title3)
                    .foregroundColor(.red)
            }
        }
    }
    
    // MARK: - Header
    
    private func HeaderArea() -> some View {
        VStack(alignment: .leading, spacing: 20) {
            Text("Complete this sentence")
                .font(.title2.bold())
            Image("Character")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding(.trailing, 50)
        }
        .padding(.top, 30)
    }
    
    // MARK: - Drag & Drop Area
    
    private func DropArea() -> some View {
        VStack(spacing: 12) {
            ForEach($rows, id: \.self) { $row in
                HStack(spacing: 10) {
                    ForEach($row) { $item in
                        Text(item.value)
                            .font(.system(size: item.fontSize))
                            .padding(.vertical, 6)
                            .padding(.horizontal, item.padding)
                            .opacity(item.isShowing ? 1 : 0)
                            .background {
                                // If item is dropped into the incorrect place or default
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .fill(item.isShowing ? .clear : .gray.opacity(0.25))
                            }
                            .background {
                                // If item is dropped into the correct place
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .stroke(.gray)
                                    .opacity(item.isShowing ? 1 : 0)
                            }
                            .onDrop(of: [.url], isTargeted: .constant(false)) { providers in
                                if let first = providers.first {
                                    let _ = first.loadObject(ofClass: URL.self) { (value, error) in
                                        guard let url = value else { return }
                                        
                                        if item.id == url.absoluteString {
                                            droppedCount += 1
                                            let progress = (droppedCount / CGFloat(characters.count))
                                            
                                            withAnimation {
                                                item.isShowing = true
                                                updateShuffledArray(for: item)
                                                self.progress = progress
                                            }
                                        } else {
                                            wrongTextDroppedAnimation()
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                                wrongTextDroppedAnimation()
                                            }
                                        }
                                    }
                                }
                                return false
                            }
                    }
                }
                rows.last != row ? Divider() : nil
            }
        }
    }
    
    private func DragArea() -> some View {
        VStack(spacing: 12) {
            ForEach(shuffledRows, id: \.self) { row in
                HStack(spacing: 10) {
                    ForEach(row) { item in
                        Text(item.value)
                            .font(.system(size: item.fontSize))
                            .padding(.vertical, 6)
                            .padding(.horizontal, item.padding)
                            .background {
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .stroke(.gray)
                            }
                            .onDrag {
                                .init(contentsOf: .init(string: item.id))!
                            }
                            .opacity(item.isShowing ? 0 : 1)
                            .background {
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .fill(item.isShowing ? .gray.opacity(0.25) : .clear)
                            }
                    }
                }
                shuffledRows.last != row ? Divider() : nil
            }
        }
    }
    
    private func generateGrid() -> [[Character]] {
        // Identifying each text width and updating it to @state variable
        characters.enumerated().forEach {
            let textSize = getTextSize(for: $0.element)
            characters[$0.offset].uploadTextSize(textSize)
        }
        
        let screenWidth = UIScreen.main.bounds.width - 30 // -30 caused by padding
        var tempArray = [Character]()
        var gridArray = [[Character]]()
        var currentWidth: CGFloat = 0
        
        characters.forEach({
            currentWidth += $0.textSize
            
            if currentWidth < screenWidth {
                tempArray.append($0)
            } else {
                gridArray.append(tempArray)
                tempArray.removeAll()
                currentWidth = $0.textSize
                tempArray.append($0)
            }
        })
        
        if !tempArray.isEmpty {
            gridArray.append(tempArray)
        }
        
        return gridArray
    }
    
    private func getTextSize(for character: Character) -> CGFloat {
        let font = UIFont.systemFont(ofSize: character.fontSize)
        let attributed = [NSAttributedString.Key.font: font]
        let size = (character.value as NSString).size(withAttributes: attributed)
        
        return size.width + (character.padding * 2) + 12 // 12 caused by VStack drag padding
    }
    
    private func updateShuffledArray(for character: Character) {
        for indice in shuffledRows.indices {
            for subIndice in shuffledRows[indice].indices {
                if shuffledRows[indice][subIndice].id == character.id {
                    shuffledRows[indice][subIndice].isShowing = true
                }
            }
        }
    }
    
    private func wrongTextDroppedAnimation() {
        withAnimation(.interactiveSpring(response: 0.3, dampingFraction: 0.2, blendDuration: 0.2)) {
            animateWrongText.toggle()
        }
    }
}

struct DuolingoView_Previews: PreviewProvider {
    static var previews: some View {
        DuolingoView()
    }
}
