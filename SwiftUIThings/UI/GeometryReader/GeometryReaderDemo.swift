//
//  GeometryReaderDemo.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import SwiftUI

struct GeometryReaderDemo: View {
    
    private let arrayOfNames = [
        "Suscríbete a SwiftBeta",
        "Aprende SwiftUI",
        "Aprende Swift",
        "Aprende Xcode",
        "SwiftUI",
        "Xcode",
        "Swift"
    ]
    
    var body: some View {
        ScrollView(showsIndicators: false) {
            VStack {
                ForEach(arrayOfNames, id: \.self) { name in
                    GeometryReader { proxy in
                        VStack {
                            Text("\(proxy.frame(in: .global).minY)")
                            Spacer()
                            Text(name)
                                .frame(width: proxy.size.width, height: 200)
                                .background(Color.green)
                                .cornerRadius(20)
                            Spacer()
                        }
                        .shadow(color: .gray, radius: 10)
                        .rotation3DEffect(
                            .degrees(Double(proxy.frame(in: .global).minY)),
                            axis: (x: 0, y: 10, z: 0)
                        )
                    } // GeometryReader
                    .frame(height: 300)
                } // ForEach
            } // VStack
            .padding([.trailing, .leading])
        } // ScrollView
    }
}

struct GeometryReaderDemo_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReaderDemo()
    }
}
