//
//  HeroDetailView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 2/4/22.
//

import SwiftUI

struct HeroDetailView: View {
    
    @State private var offset: CGFloat = 0
    @State private var showDetailContent = false
    
    private let movie: Movie
    private let animation: Namespace.ID
    @Binding private var showDetailView: Bool
    @Binding private var currentCardSize: CGSize
    
    init(movie: Movie,
         animation: Namespace.ID,
         showDetailView: Binding<Bool>,
         currentCardSize: Binding<CGSize>) {
        self.movie = movie
        self.animation = animation
        self._showDetailView = showDetailView
        self._currentCardSize = currentCardSize
    }
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                Image(movie.artwork)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: currentCardSize.width, height: currentCardSize.height)
                    .cornerRadius(15)
                    .matchedGeometryEffect(id: movie.id, in: animation)
                
                VStack(spacing: 15) {
                    Text("Story Plot")
                        .font(.largeTitle)
                        .fontWeight(.semibold)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.top, 25)
                    
                    Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
                        .multilineTextAlignment(.leading)
                    
                    Button {
                        
                    } label: {
                        Text("Book Ticket")
                            .fontWeight(.semibold)
                            .foregroundColor(.white)
                            .padding(.vertical)
                            .frame(maxWidth: .infinity)
                            .background {
                                RoundedRectangle(cornerRadius: 10, style: .continuous)
                                    .fill(.blue)
                            }
                    }
                    .padding(.top, 20)
                }
                .opacity(showDetailContent ? 1 : 0)
                .offset(y: showDetailContent ? 0 : 200)
            }
            .padding()
            .modifier(OffsetModifier(offset: $offset))
        }
        .coordinateSpace(name: "SCROLL")
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background {
            Rectangle()
                .fill(.ultraThinMaterial)
                .ignoresSafeArea()
        }
        .onAppear {
            UIScrollView.appearance().bounces = true
            withAnimation(.easeInOut) { showDetailContent = true }
        }
        .onChange(of: offset) { newValue in
            if newValue > 100 {
                withAnimation(.easeInOut) { showDetailContent = false }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                    withAnimation(.easeInOut) { showDetailView = false }
                }
            }
        }
    }
}

struct HeroDetailView_Previews: PreviewProvider {
    static var previews: some View {
        HeroView()
            .preferredColorScheme(.dark)
    }
}
