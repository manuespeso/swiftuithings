//
//  HeroView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 1/4/22.
//

import SwiftUI

struct HeroView: View {
    // MARK: - Animated View Properties
    @State private var currentIndex = 0
    @State private var currentTab = "Films"
    @State private var detailMovie: Movie?
    @State private var showDetailView = false
    // FOR MATCHED GEOMETRY EFFECT STORING CURRENT CARD SIZE
    @State private var currentCardSize: CGSize = .zero
    
    @Namespace private var animation
    @Environment(\.colorScheme) private var scheme
    
    private let movies: [Movie] = [
        Movie(movieTitle: "Ad Astra", artwork: "Movie1"),
        Movie(movieTitle: "Star Wars", artwork: "Movie2"),
        Movie(movieTitle: "Toy Story 4", artwork: "Movie3"),
        Movie(movieTitle: "Lion King", artwork: "Movie4"),
        Movie(movieTitle: "Spider Man No Way Home", artwork: "Movie5"),
        Movie(movieTitle: "Shang Chi", artwork: "Movie6"),
        Movie(movieTitle: "Hawkeye", artwork: "Movie7"),
    ]
    
    var body: some View {
        ZStack {
            BlurredBackgroundView()
            // MARK: - Main View Content
            VStack {
                CustomNavBar()
                CarouselContent()
                CustomIndicator()
                CustomFooter()
            }
            .overlay {
                if let movie = detailMovie, showDetailView {
                    HeroDetailView(
                        movie: movie,
                        animation: animation,
                        showDetailView: $showDetailView,
                        currentCardSize: $currentCardSize
                    )
                }
            }
        }
        .defaultNavigationBarStyle()
    }
    // MARK: - Blurred BG
    private func BlurredBackgroundView() -> some View {
        GeometryReader { proxy in
            TabView(selection: $currentIndex) {
                ForEach(movies.indices, id: \.self) { index in
                    Image(movies[index].artwork)
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .frame(width: proxy.size.width, height: proxy.size.height)
                        .clipped()
                        .tag(index)
                }
            }
            .tabViewStyle(.page(indexDisplayMode: .never))
            .animation(.easeInOut, value: currentIndex)
            
            let color: Color = scheme == .dark ? .black : .white
            //Custom Gradient
            LinearGradient(
                colors: [
                    .black,
                    .clear,
                    color.opacity(0.15),
                    color.opacity(0.5),
                    color.opacity(0.8),
                    color,
                    color
                ],
                startPoint: .top,
                endPoint: .bottom
            )
            // Blurred Overlay
            Rectangle()
                .fill(.ultraThinMaterial)
        }
        .ignoresSafeArea()
    }
    // MARK: - Blurred BG
    private func CarouselContent() -> some View {
        SnapCarousel(index: $currentIndex, spacing: 20, trailingSpace: 110, items: movies) { movie in
            GeometryReader { proxy in
                let size = proxy.size
                
                Image(movie.artwork)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: size.width, height: size.height)
                    .cornerRadius(15)
                    .matchedGeometryEffect(id: movie.id, in: animation)
                    .onTapGesture {
                        currentCardSize = size
                        detailMovie = movie
                        withAnimation(.easeInOut) { showDetailView = true }
                    }
            }
        }
        .padding(.top, 70)
    }
    // MARK: - Custom Nav Bar
    private func CustomNavBar() -> some View {
        HStack(spacing: 0) {
            ForEach(["Films", "Localities"], id: \.self) { tab in
                Button {
                    withAnimation { currentTab = tab }
                } label: {
                    Text(tab)
                        .foregroundColor(.white)
                        .padding(.vertical, 6)
                        .padding(.horizontal, 20)
                        .background {
                            if currentTab == tab {
                                Capsule()
                                    .fill(.regularMaterial)
                                    .environment(\.colorScheme, .dark)
                                    .matchedGeometryEffect(id: "TAB", in: animation)
                            }
                        }
                }

            }
        }
    }
    // MARK: - Custom Indicator
    private func CustomIndicator() -> some View {
        HStack(spacing: 5) {
            ForEach(movies.indices, id: \.self) { index in
                Circle()
                    .fill(currentIndex == index ? .blue : .gray.opacity(0.5))
                    .frame(
                        width: currentIndex == index ? 10 : 6,
                        height: currentIndex == index ? 10 : 6
                    )
            }
        }
        .animation(.easeInOut, value: currentIndex)
    }
    // MARK: - Custom Footer
    private func CustomFooter() -> some View {
        VStack {
            HStack {
                Text("Popular")
                    .font(.title3.bold())
                Spacer()
                Button("See More") {}
                .font(.system(size: 16, weight: .semibold))
            }
            .padding()
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 15) {
                    ForEach(movies) { movie in
                        Image(movie.artwork)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 100, height: 120)
                            .cornerRadius(15)
                    }
                }
                .padding()
            }
        }
    }
}

struct HeroView_Previews: PreviewProvider {
    static var previews: some View {
        HeroView()
            .preferredColorScheme(.dark)
    }
}
