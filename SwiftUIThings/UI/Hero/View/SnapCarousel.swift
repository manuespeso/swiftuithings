//
//  SnapCarousel.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 1/4/22.
//

import SwiftUI

struct SnapCarousel<Content: View, T: Identifiable>: View {
    
    @GestureState private var offset: CGFloat = 0
    @State private var currentIndex: Int = 0
    
    @Binding private var index: Int
    private let spacing: CGFloat
    private let trailingSpace: CGFloat
    private let list: [T]
    private let content: (T) -> Content
    
    init(index: Binding<Int>,
         spacing: CGFloat = 15,
         trailingSpace: CGFloat = 100,
         items: [T],
         @ViewBuilder content: @escaping (T) -> Content) {
        self._index = index
        self.spacing = spacing
        self.trailingSpace = trailingSpace
        self.list = items
        self.content = content
    }
    
    var body: some View {
        GeometryReader { proxy in
            let width = proxy.size.width - (trailingSpace - spacing)
            let adjustMentWidth = (trailingSpace / 2) - spacing
            
            HStack(spacing: spacing) {
                ForEach(list) { item in
                    content(item)
                        .frame(width: proxy.size.width - trailingSpace)
                        .offset(y: getOffset(item: item, width: width))
                }
            }
            .padding(.horizontal, spacing)
            .offset(x: (CGFloat(currentIndex) * -width) + (currentIndex != 0 ? adjustMentWidth : 0) + offset)
            .gesture(
                DragGesture()
                    .updating($offset, body: { value, out, _ in
                        out = (value.translation.width / 1.5)
                    })
                    .onEnded({ value in
                        //Updating current index...
                        let offsetX = value.translation.width
                        // Convert the tranlsation into progress (0 - 1) and round the value based on the progress increasing or decreasing the currentIndex...
                        let progress = -offsetX / width
                        let roundIndex = progress.rounded()
                        // Setting min...
                        currentIndex = max(min(currentIndex + Int(roundIndex), list.count - 1), 0)
                        // Updating index...
                        currentIndex = index
                    })
                    .onChanged({ value in
                        // Updating only index....
                        let offsetX = value.translation.width
                        // Convert the tranlsation into progress (0 - 1) and round the value based on the progress increasing or decreasing the currentIndex...
                        let progress = -offsetX / width
                        let roundIndex = progress.rounded()
                        // Setting min...
                        index = max(min(currentIndex + Int(roundIndex), list.count - 1), 0)
                    })
            )
        }
        // Animation when offset = 0
        .animation(.easeInOut, value: offset == 0)
    }
    // Moving View based on scroll Offset...
    private func getOffset(item: T, width: CGFloat) -> CGFloat {
        // Shifting Current Item to Top...
        let progress = ((offset < 0 ? offset : -offset) / width) * 60
        // Max 60... then again minus from 60
        let topOffset = -progress < 60 ? progress : -(progress + 120)
        let previous = getIndex(item: item) - 1 == currentIndex ? (offset < 0 ? topOffset : -topOffset) : 0
        let next = getIndex(item: item) + 1 == currentIndex ? (offset < 0 ? -topOffset : topOffset) : 0
        // Saftey check between 0 to max list size...
        let checkBetween = currentIndex >= 0 && currentIndex < list.count ? (getIndex(item: item) - 1 == currentIndex ? previous : next) : 0
        // Checking current...
        // If so shifting view to top...
        return getIndex(item: item) == currentIndex ? (-60 - topOffset) : checkBetween
    }
    // Fetching Index...
    private func getIndex(item: T) -> Int {
        list.firstIndex { $0.id == item.id } ?? 0
    }
}
