//
//  MessagesGradientDemo.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 22/6/22.
//

import SwiftUI

struct MessagesGradientDemo: View {
    let strings = """
    One of my favorite little iOS touches is the screen-space gradient in Messages.app.
    It's super easy to build in SwiftUI:
    First, we place a `.coordinateSpace` modifier outside the `ScrollView`.
    Then we use a `GeometryReader` to resolve the frame of the message in this coordinate space.
    By inverting the frame's `y`-coordiante and forcing its `height` to the desired length of the gradient, we can calcuate the position of the gradient relative to the message's coordinate space.
    Finally, we pass this `CGRect` to the gradient's `in(_:)` method and use the resulting `ShapeStyle` to fill the message's background.
    Done!
    Texto largo
    Para que haya scroll
    Que si no no funciona
    La demo que quiero ver
    """.split(separator: "\n").map({ String($0) })
    
    var body: some View {
        let linearGradient = LinearGradient(
            colors: [.blue, .purple],
            startPoint: .top,
            endPoint: .bottom
        )
        
        let backdrop = GeometryReader { proxy in
            let rect: CGRect = {
                var frame = proxy.frame(in: .global)
                frame.size.height = UIScreen.main.bounds.height - 150
                frame.origin.y = -frame.origin.y + 25
                return frame
            }()
            
            RoundedRectangle(cornerRadius: 16, style: .continuous)
                .fill(linearGradient.in(rect))
                .drawingGroup()
        }
        
        ScrollView {
            VStack(alignment: .trailing, spacing: 2) {
                Text("Today 19:32")
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding(.bottom, 6)
                
                ForEach(strings, id: \.self) { string in
                    let text = (try? AttributedString(markdown: string)) ?? .init(string)
                    Text(text)
                        .font(.system(.body, design: .rounded))
                        .foregroundColor(.white)
                        .padding(12)
                        .background(backdrop)
                        .frame(maxWidth: 320, alignment: .trailing)
                }
                .frame(maxWidth: .infinity, alignment: .trailing)
                
                Text("Read")
            }
            .padding(.horizontal, 12)
        }
        .font(.system(.subheadline, design: .rounded))
        .foregroundColor(.secondary)
        .defaultNavigationBarStyle()
    }
}

struct MessagesGradientDemo_Previews: PreviewProvider {
    static var previews: some View {
        MessagesGradientDemo()
    }
}
