
import Foundation

struct ParallaxCardModel: Identifiable {
    
    static let fakeData: [ParallaxCardModel] = [
        .init(name: "Brazil", frontImage: "rio", backgroundImage: "rio-bg"),
        .init(name: "France", frontImage: "france", backgroundImage: "france-bg"),
        .init(name: "Iceland", frontImage: "iceland", backgroundImage: "iceland-bg")
    ]
    
    let id = UUID()
    let name: String
    let frontImage: String
    let backgroundImage: String
}
