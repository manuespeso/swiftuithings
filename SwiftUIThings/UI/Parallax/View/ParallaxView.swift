
import CoreMotion
import SwiftUI

struct ParallaxView: View {
    
    private let motionManager = CMMotionManager()
    private let queue = OperationQueue()
    
    @State private var roll: Double = .zero
    
    var body: some View {
        VStack {
            TabView {
                ForEach(ParallaxCardModel.fakeData) { card in
                    ZStack {
                        Image(card.backgroundImage)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .offset(x: -roll * 60)
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                            .clipped()
                        
                        Image(card.frontImage)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .offset(x: roll * 5)
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                            .scaleEffect(1.1)
                            .clipped()
                        
                        VStack {
                            Text(card.name)
                                .padding(.top, 30)
                                .font(.system(size: 34, weight: .black, design: .rounded))
                                .foregroundColor(Color.darknessWhite.opacity(0.8))
                                .offset(x: -roll * 20)
                            Spacer()
                        }
                    }
                }
                .clipShape(RoundedRectangle(cornerRadius: 20))
                .padding(.horizontal, 40)
                .padding(.top, 20)
                .padding(.bottom, 50)
                .onAppear {
                    self.motionManager.deviceMotionUpdateInterval = 1/60
                    self.motionManager.startDeviceMotionUpdates(to: self.queue) { (data, error) in
                        guard let data = data else { return }
                        let attitude: CMAttitude = data.attitude
                        print("roll: \(attitude.roll)")
                        
                        DispatchQueue.main.async {
                            withAnimation(.spring(response: 0.33, dampingFraction: 0.33)) {
                                self.roll = attitude.roll
                            }
                        }
                    }
                }
                .onDisappear { motionManager.stopDeviceMotionUpdates() }
            }
            .tabViewStyle(.page)
        }
        .background {
            Rectangle()
                .fill(Color.darknessPurple)
                .ignoresSafeArea()
        }
    }
}

struct ParallaxView_Previews: PreviewProvider {
    static var previews: some View {
        ParallaxView()
    }
}
