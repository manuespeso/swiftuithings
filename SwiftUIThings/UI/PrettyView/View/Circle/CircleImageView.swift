//
//  CircleImage.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 22/3/22.
//

import SwiftUI

struct CircleImageView: View {
    var image: Image

    var body: some View {
        image
            .resizable()
            .frame(width: 300, height: 300)
            .clipShape(Circle())
            .overlay {
                Circle().stroke(.white, lineWidth: 4)
            }
            .shadow(radius: 7)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImageView(image: Image("prettyImage"))
    }
}

