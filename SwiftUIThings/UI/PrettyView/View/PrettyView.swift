//
//  PrettyView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 23/3/22.
//

import SwiftUI
import CoreLocation

struct PrettyView: View {
    
    @State private var password = ""
    
    private let landmark: Landmark
    
    init(landmark: Landmark) {
        self.landmark = landmark
    }
    
    var body: some View {
        ScrollView {
            MapView(coordinate: CLLocationCoordinate2D(latitude: landmark.latitude,
                                                       longitude: landmark.longitude))
                .frame(height: 300)
            
            CircleImageView(image: Image(landmark.imageName))
                .offset(y: -130)
                .padding(.bottom, -130)
            
            VStack(alignment: .leading) {
                Text(landmark.name)
                    .font(.title)
                
                HStack {
                    Text(landmark.park)
                    Spacer()
                    Text(landmark.state)
                }
                .font(.subheadline)
                .foregroundColor(.secondary)
                
                Divider()
                
                Text("About \(landmark.name)")
                    .font(.title2)
                Text(landmark.description)
            }
            .padding()
            
            PasswordTextField(password: $password)
                .padding()
        }
        .edgesIgnoringSafeArea(.all)
        .defaultNavigationBarStyle(with: landmark.name)
    }
}

struct PrettyView_Previews: PreviewProvider {
    static var previews: some View {
        let landmark = Landmark(
            id: 1,
            name: "peuwba",
            park: "pepe",
            state: "eymajo",
            description: "ijiji",
            imageName: "prettyImage",
            latitude: 34.011_286,
            longitude: -116.166_868
        )
        PrettyView(landmark: landmark)
    }
}
