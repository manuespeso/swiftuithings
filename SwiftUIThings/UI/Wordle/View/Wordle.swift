//
//  Wordle.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import SwiftUI

struct Wordle: View {
    /**
     - @StateObject -> Si se repinta la vista, el objeto NO se regenera, es el mismo
     - @ObservedObject -> Si se repinta la vista, el objeto SI se regenera, NO es el mismo
     */
    @StateObject private var viewModel = WordleViewModel()
    
    var body: some View {
        ZStack {
            VStack(spacing: 40) {
                WordleGame(viewModel: viewModel)
                WordleKeyboard(viewModel: viewModel)
            }
            
            if let bannerType = viewModel.bannerType {
                WordleBannerView(bannerType: bannerType)
            }
        }
    }
}

struct Wordle_Previews: PreviewProvider {
    static var previews: some View {
        Wordle()
    }
}
