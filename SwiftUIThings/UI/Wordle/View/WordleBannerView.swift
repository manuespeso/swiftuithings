//
//  WordleBannerView.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import SwiftUI

struct WordleBannerView: View {
    
    @State private var isOnScreen: Bool = false
    private let bannerType: BannerType
    
    init(bannerType: BannerType) {
        self.bannerType = bannerType
    }
    
    var body: some View {
        VStack {
            Spacer()
            switch bannerType {
            case .error(let errorMessage):
                Text(errorMessage)
                    .foregroundColor(.white)
                    .padding()
                    .background(.red)
                    .cornerRadius(12)
            case .success:
                Text("¡HAS GANADO!")
                    .foregroundColor(.white)
                    .padding()
                    .background(.blue)
                    .cornerRadius(12)
            }
            Spacer()
        }
        .padding(.horizontal, 12)
        .frame(height: 40)
        .animation(.easeInOut(duration: 0.3), value: isOnScreen)
        .offset(y: isOnScreen ? -350 : -500)
        .onAppear { isOnScreen = true }
    }
}

struct WordleBannerView_Previews: PreviewProvider {
    static var previews: some View {
        WordleBannerView(bannerType: .success)
    }
}
