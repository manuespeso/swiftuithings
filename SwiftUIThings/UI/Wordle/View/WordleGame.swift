//
//  WordleGame.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import SwiftUI

struct WordleGame: View {
    
    @ObservedObject private var viewModel: WordleViewModel
    
    private let columns: [GridItem] = Array(repeating: GridItem(.flexible(minimum: 20), spacing: 0), count: 5)
    
    init(viewModel: WordleViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        LazyVGrid(columns: columns, spacing: 8) {
            ForEach(0...5, id: \.self) { index in
                ForEach(viewModel.gameData[index], id: \.id) { model in
                    Text(model.name)
                        .font(.system(size: 40, weight: .bold))
                        .frame(width: 60, height: 60)
                        .foregroundColor(viewModel.hasError(index: index) ? .white : model.foregroundColor)
                        .background(viewModel.hasError(index: index) ? .red : model.backgroundColor)
                }
            }
        }
        .padding(.horizontal, 20)
    }
}

struct WordleGame_Previews: PreviewProvider {
    static var previews: some View {
        WordleGame(viewModel: WordleViewModel())
    }
}
