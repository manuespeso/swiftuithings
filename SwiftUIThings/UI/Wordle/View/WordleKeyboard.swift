//
//  WordleKeyboard.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import SwiftUI

struct WordleKeyboard: View {
    
    @ObservedObject private var viewModel: WordleViewModel
    
    private let columns: [GridItem] = Array(repeating: GridItem(.flexible(minimum: 20), spacing: 0), count: 10)
    
    init(viewModel: WordleViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        LazyVGrid(columns: columns, spacing: 12) {
            ForEach(viewModel.keyboardData, id: \.id) { model in
                Button {
                    viewModel.addNewLetter(model: model)
                } label: {
                    Text(model.name)
                        .font(.body)
                }
                .frame(width: 34, height: 50)
                .foregroundColor(model.foregroundColor)
                .background(model.backgroundColor)
                .cornerRadius(8)
            }
        }
        .padding(.horizontal, 8)
    }
}

struct WordleKeyboard_Previews: PreviewProvider {
    static var previews: some View {
        WordleKeyboard(viewModel: WordleViewModel())
    }
}
