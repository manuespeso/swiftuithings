//
//  WordleViewModel.swift
//  SwiftUIThings
//
//  Created by Manuel Espeso on 27/3/22.
//

import Foundation
import UIKit

enum BannerType {
    case error(String)
    case success
}

final class WordleViewModel: ObservableObject {
    
    // MARK: - Variables
    
    // Nos servirá para saber si necesitamos mostrar una alerta o no, y de que tipo
    @Published var bannerType: BannerType? = nil
    /*
     - Representación de nuestro tablero de juego.
     - Al inicializar el juego este tablero está vacío, pero a medida que el user añada palabras se irá rellenando.
     */
    @Published var gameData: [[WordleLetterModel]] = [
        [.init(""), .init(""), .init(""), .init(""), .init("")],
        [.init(""), .init(""), .init(""), .init(""), .init("")],
        [.init(""), .init(""), .init(""), .init(""), .init("")],
        [.init(""), .init(""), .init(""), .init(""), .init("")],
        [.init(""), .init(""), .init(""), .init(""), .init("")],
        [.init(""), .init(""), .init(""), .init(""), .init("")]
    ]
    // Representación de nuestro teclado.
    @Published var keyboardData: [WordleLetterModel] = [
        .init("Q"), .init("W"), .init("E"),
        .init("R"), .init("T"), .init("Y"),
        .init("U"), .init("I"), .init("O"),
        .init("P"), .init("A"), .init("S"),
        .init("D"), .init("F"), .init("G"),
        .init("H"), .init("J"), .init("K"),
        .init("L"), .init("Ñ"), .init("🚀"),
        .init(""), .init("Z"), .init("X"),
        .init("C"), .init("B"), .init("N"),
        .init("M"), .init(""), .init("🗑")
    ]
    // Nos servirá para saber en qué fila estamos de nuestro Grid
    private var numOfRow: Int = 0
    // Nos servirá para hacer comprobaciones a la palabra que se está insertando
    private var word: [WordleLetterModel] = []
    // Palabra para matchear
    private let result = "REINA"
    
    // MARK: - Public
    
    func addNewLetter(model: WordleLetterModel) {
        bannerType = nil
        
        if model.name == "🚀" {
            tapOnSend()
            return
        }
        if model.name == "🗑" {
            tapOnRemove()
            return
        }
        if word.count < 5 && numOfRow < gameData.count {
            let letter = WordleLetterModel(model.name)
            word.append(letter)
            gameData[numOfRow][word.count-1] = letter
        }
    }
    
    func hasError(index: Int) -> Bool {
        guard let bannerType = bannerType else { return false }

        switch bannerType {
        case .error(_):
            return index == numOfRow
        case .success:
            return false
        }
    }
    
    // MARK: - Private
    
    private func tapOnRemove() {
        guard word.count > 0 else { return }
        
        gameData[numOfRow][word.count-1] = .init("")
        word.removeLast()
    }
    
    private func tapOnSend() {
        guard word.count == 5 else {
            bannerType = .error("¡Añade más letras!")
            return
        }
        
        let finalStringWord = word.map { $0.name }.joined()
        
        if wordIsReal(finalStringWord) {
            searchingWordMatch()
            handleUserWinning()
        } else {
            bannerType = .error("La palabra no existe")
        }
    }
    
    private func wordIsReal(_ word: String) -> Bool {
        UIReferenceLibraryViewController.dictionaryHasDefinition(forTerm: word)
    }
    
    private func searchingWordMatch() {
        for (index, _) in word.enumerated() {
            let currentCharacter = word[index].name
            var status: WordleStatus
            
            if result.contains(where: { String($0) == currentCharacter }) {
                status = .appear
                print("\(currentCharacter) Appear")
                
                if currentCharacter == String(result[result.index(result.startIndex, offsetBy: index)]) {
                    status = .match
                    print("\(word[index].name) MATCH ✅")
                }
            } else {
                status = .dontAppear
                print("\(word[index].name) DONT Appear")
            }
            
            updateGameView(for: index, status: status)
            updateKeyboardView(for: index, status: status)
        }
    }
    
    private func updateGameView(for index: Int, status: WordleStatus) {
        var updateGameBoardCell = gameData[numOfRow][index]
        updateGameBoardCell.status = status
        
        gameData[numOfRow][index] = updateGameBoardCell
    }
    
    private func updateKeyboardView(for index: Int, status: WordleStatus) {
        guard let indexToUpdate = keyboardData.firstIndex(where: { $0.name == word[index].name }) else { return }
        
        var keyboardKey = keyboardData[indexToUpdate]
        if keyboardKey.status != .match {
            keyboardKey.status = status
            keyboardData[indexToUpdate] = keyboardKey
        }
    }
    
    private func handleUserWinning() {
        let isUserWinner = gameData[numOfRow].reduce(0) { partialResult, letterModel in
            if letterModel.status == .match {
                return partialResult + 1
            }
            return 0
        }
        
        if isUserWinner == 5 {
            bannerType = .success
        } else {
            // Clean word and move to the next row
            word = []
            numOfRow += 1
        }
    }
}
